# dev setup for allcongress.com

Currently we work on avicenna.allcongress.com VM with IP address 91.134.112.93

Every developer should have hers/his subsite to work. So for example panos should work at panos.dev.allcongress.com

## SFTP/ssh & folder structure
There are credentials for sftp & ssh access like this

server: avicenna.allcongress.com
user: o1.USER
pass: {mysecretpass}

{o1.panosperis/miR9aHrF}
{o1.kl/I2NB3m4z}


the folder structure at the server is
-- sites
      -- panos.allcongress.com
          -- root of subsite with modules, themes, files folders

note that the drupal root and the common modules & themes are not accessible by the dev

when we are at the root of the subsite we can use drush commands like drush uli, drush cc all, drush sql-dump > files/dbexport.sql


## Web panel / Aegir

Login to the aegir webpanel and to do tasks like site backup, site cloning etc Credentials have been sent via email